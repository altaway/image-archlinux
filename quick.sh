#!/usr/bin/env zsh

setopt NULL_GLOB
trap "print -P \"%B\nCTRL-C%b\"; exit 5" 2

[[ ${EUID} -ne 0 ]] && {
	echo "This script must be run as root."
	exit 1
}

{
	print -P "%B%F{green}trying option 1%f%b"
	rm -vrf out/ work/build.*
	./build.sh -v
} || {
	print -P "%B%F{yellow}1 failed, %F{green}trying 2%f%b"
	rm -vrf out/ work/build
	./build.sh -v
} || {
	print -P "%B%F{yellow}2 failed, %F{green}trying 3%f%b"
	paccache -rk0
	pacman -Syy
	rm -vrf out/ work/build.*
	./build.sh -v
} || {
	print -P "%B%F{yellow}3 failed, %F{red}you screwed up%f%b"
	exit 5
}

[[ -n $1 ]] && {
	MEDIA=$1
	print -P "%B%F{green}copying to removable media $MEDIA%f%b"
	dd if=out/archlinux-$(date +%Y.%m.%d)-x86_64.iso of=$MEDIA
}
