#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
pushd /root/.dotfiles
stow -t /root/ -R .
popd
chmod 700 /root
passwd -d root

# Add the user and run relevant commands
USER=user
UHOME=/home/$USER
userdel -rf $USER || true
useradd -ms /usr/bin/zsh $USER
cp -aT /etc/skel $UHOME
chmod 751 $UHOME
chown -R $USER:$USER $UHOME
passwd -d $USER
DOTFILES=$UHOME/.dotfiles
pushd $DOTFILES
stow -t $UHOME -R .
popd
chown -R $USER:$USER $UHOME
chown -R root:root /etc/doas.conf /etc/sudoers.d # else, the commands don't work
zsh -c "echo \"\n$USER ALL=(ALL) NOPASSWD: ALL\" >> /etc/sudoers"; \
zsh -c "echo \"\npermit nopass keepenv user\" >> /etc/doas.conf"

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default multi-user.target

# netctl and similar commands go here
systemctl enable NetworkManager.service
